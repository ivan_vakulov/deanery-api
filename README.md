# deanery-api

### Installation

```sh
$ cd deanery-api
$ composer install
$ php artisan migrate:refresh --seed
$ php artisan serve
```

