<?php

use Illuminate\Database\Seeder;

class UniversityGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universityGroup = new \App\UniversityGroup();
        $universityGroup->group_number = 'TestGroupNumber';
        $universityGroup->save();
    }
}
