<?php

use Illuminate\Database\Seeder;

class LecturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lecturer = new \App\Lecturer();
        $lecturer->full_name = 'Test Name';
        $lecturer->position = 'Director';
        $lecturer->phone_number = '0987654321';
        $lecturer->department()->associate(\App\Department::find(1));
        $lecturer->save();
    }
}
