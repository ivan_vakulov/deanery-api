<?php

use Illuminate\Database\Seeder;

class GroupRecordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupRecord = new \App\GroupRecord();
        $groupRecord->student()->associate(\App\Student::find(1));
        $groupRecord->universityGroup()->associate(\App\UniversityGroup::find(1));
        $groupRecord->discipline()->associate(\App\Discipline::find(1));
        $groupRecord->save();
    }
}
