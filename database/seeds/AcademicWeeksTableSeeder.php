<?php

use Illuminate\Database\Seeder;

class AcademicWeeksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $academicWeek = new \App\AcademicWeek();
        $academicWeek->week_num = 1;
        $academicWeek->start_date = \Carbon\Carbon::parse('2000-01-01');
        $academicWeek->end_date = \Carbon\Carbon::parse('2000-01-01');
        $academicWeek->semester = 'Semester';
        $academicWeek->save();
    }
}
