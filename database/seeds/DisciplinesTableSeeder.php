<?php

use Illuminate\Database\Seeder;

class DisciplinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discipline = new \App\Discipline();
        $discipline->name = 'Discipline test name';
        $discipline->selfWorkTime = 50;
        $discipline->specialty()->associate(\App\Specialty::find(1));
        $discipline->save();

        $disciplineNew = new \App\Discipline();
        $disciplineNew->name = 'Discipline 2 test name';
        $disciplineNew->selfWorkTime = 50;
        $disciplineNew->specialty()->associate(\App\Specialty::find(1));
        $disciplineNew->preDiscipline()->associate(\App\Discipline::find(1));
        $disciplineNew->save();
    }
}
