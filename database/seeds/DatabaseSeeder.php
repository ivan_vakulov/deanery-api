<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(AuditoriesTableSeeder::class);
        $this->call(LecturersTableSeeder::class);
        $this->call(SpecialtiesTableSeeder::class);
        $this->call(DisciplinesTableSeeder::class);
        $this->call(UniversityClassesTableSeeder::class);
        $this->call(AcademicWeeksTableSeeder::class);
        $this->call(ClassTimesTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(UniversityGroupsTableSeeder::class);
        $this->call(UniversitySchedulesTableSeeder::class);
        $this->call(GroupRecordsTableSeeder::class);
    }
}
