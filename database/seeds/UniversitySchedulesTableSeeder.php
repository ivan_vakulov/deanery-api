<?php

use Illuminate\Database\Seeder;

class UniversitySchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universitySchedule = new \App\UniversitySchedule();
        $universitySchedule->week_day = 5;
        $universitySchedule->classTime()->associate(\App\ClassTime::find(1));
        $universitySchedule->academicWeek()->associate(\App\AcademicWeek::find(1));
        $universitySchedule->auditory()->associate(\App\Auditory::find(1));
        $universitySchedule->universityClass()->associate(\App\UniversityClass::find(1));
        $universitySchedule->group()->associate(\App\UniversityGroup::find(1));
        $universitySchedule->save();
    }
}
