<?php

use Illuminate\Database\Seeder;

class UniversityClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universityClass = new \App\UniversityClass();
        $universityClass->class_type = 'Test class type';
        $universityClass->discipline()->associate(\App\Discipline::find(1));
        $universityClass->lecturer()->associate(\App\Lecturer::find(1));
        $universityClass->save();
    }
}
