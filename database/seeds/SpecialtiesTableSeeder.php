<?php

use Illuminate\Database\Seeder;

class SpecialtiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $specialty = new \App\Specialty();
        $specialty->name = 'Specialty test name';
        $specialty->save();
    }
}
