<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student = new \App\Student();
        $student->name = 'Test Student Name';
        $student->start_university = \Carbon\Carbon::parse('2000-01-01');
        $student->end_university = \Carbon\Carbon::parse('2000-01-01');
        $student->phone = '0987654321';
        $student->end_reason = 'Test reason';
        $student->specialty()->associate(\App\Specialty::find(1));
        $student->save();
    }
}
