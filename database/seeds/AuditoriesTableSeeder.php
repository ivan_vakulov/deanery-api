<?php

use Illuminate\Database\Seeder;

class AuditoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $auditory = new \App\Auditory();
        $auditory->number = '101';
        $auditory->building_number = '102';
        $auditory->capacity = 20;
        $auditory->department()->associate(\App\Department::find(1));
        $auditory->save();
    }
}
