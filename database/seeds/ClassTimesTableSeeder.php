<?php

use Illuminate\Database\Seeder;

class ClassTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classTime = new \App\ClassTime();
        $classTime->start_time = '2000-01-01 00:00:00';
        $classTime->end_time = '2000-01-01 00:00:00';
        $classTime->save();
    }
}
