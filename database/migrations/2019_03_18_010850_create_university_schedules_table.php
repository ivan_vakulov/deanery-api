<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitySchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('week_day');
            $table->bigInteger('university_class_id')->unsigned();
            $table->foreign('university_class_id')->references('id')->on('university_classes')->onDelete('cascade');
            $table->bigInteger('class_time_id')->unsigned();
            $table->foreign('class_time_id')->references('id')->on('class_times')->onDelete('cascade');
            $table->bigInteger('academic_week_id')->unsigned();
            $table->foreign('academic_week_id')->references('id')->on('academic_weeks')->onDelete('cascade');
            $table->bigInteger('auditory_id')->unsigned();
            $table->foreign('auditory_id')->references('id')->on('auditories')->onDelete('cascade');
            $table->bigInteger('university_group_id')->unsigned();
            $table->foreign('university_group_id')->references('id')->on('university_groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_schedules');
    }
}
