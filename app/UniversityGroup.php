<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityGroup extends Model
{
    public function universitySchedules() {
        return $this->hasMany('App\UniversitySchedule', 'university_group_id', 'id');
    }
}
