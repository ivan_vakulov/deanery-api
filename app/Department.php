<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function auditories() {
        return $this->hasMany('App\Auditory', 'department_id', 'id');
    }

    public function lecturers() {
        return $this->hasMany('App\Lecturer', 'department_id', 'id');
    }
}
