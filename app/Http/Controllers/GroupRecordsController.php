<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\GroupRecord;
use App\Student;
use App\UniversityGroup;
use Illuminate\Http\Request;

class GroupRecordsController extends Controller
{
    public function getGroupRecordById(Request $request, $id) {
        return response()->json([
            'data' => GroupRecord::where(['id' => $id])->with('student')->with('universityGroup')->with('discipline')->first(),
            'status' => true,
        ], 200);
    }

    public function getGroupRecords(Request $request) {
        $groupRecord = GroupRecord::with('student')->with('universityGroup')->with('discipline')->get();

        return response()->json([
            'data' => $groupRecord,
            'status' => true
        ], 200);
    }

    public function createGroupRecord(Request $request) {
        $this->validate($request, [
            'student_id' => 'required',
            'university_group_id' => 'required',
            'discipline_id' => 'required',
        ]);

        $groupRecord = new GroupRecord();
        $groupRecord->student()->associate(Student::find($request->student_id));
        $groupRecord->universityGroup()->associate(UniversityGroup::find($request->university_group_id));
        $groupRecord->discipline()->associate(Discipline::find($request->discipline_id));
        $groupRecord->save();

        return response()->json([
            'groupRecord' => $groupRecord,
            'status' => true
        ], 200);
    }

    public function updateGroupRecord(Request $request, $id) {
        $this->validate($request, [
            'student_id' => 'required',
            'university_group_id' => 'required',
            'discipline_id' => 'required',
        ]);

        $groupRecord = GroupRecord::find($id);
        $groupRecord->student()->associate(Student::find($request->student_id));
        $groupRecord->universityGroup()->associate(UniversityGroup::find($request->university_group_id));
        $groupRecord->discipline()->associate(Discipline::find($request->discipline_id));
        $groupRecord->save();

        return response()->json([
            'groupRecord' => $groupRecord,
            'status' => true
        ], 200);
    }

    public function deleteGroupRecord(Request $request, $id) {
        $groupRecord = GroupRecord::find($id);
        $groupRecord->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
