<?php

namespace App\Http\Controllers;

use App\AcademicWeek;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AcademicWeeksController extends Controller
{
    public function getAcademicWeekById(Request $request, $id) {
        return response()->json([
            'data' => AcademicWeek::where(['id' => $id])->first(),
            'status' => true,
        ], 200);
    }

    public function getAcademicWeeks(Request $request) {
        $academicWeeks = AcademicWeek::all();

        return response()->json([
            'data' => $academicWeeks,
            'status' => true
        ], 200);
    }

    public function createAcademicWeek(Request $request) {
        $this->validate($request, [
            'week_num' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'semester' => 'required',
        ]);

        $academicWeek = new AcademicWeek();
        $academicWeek->week_num = $request->week_num;
        $academicWeek->start_date = Carbon::parse($request->start_date);
        $academicWeek->end_date = Carbon::parse($request->end_date);
        $academicWeek->semester = $request->semester;
        $academicWeek->save();

        return response()->json([
            'academicWeek' => $academicWeek,
            'status' => true
        ], 200);
    }

    public function updateAcademicWeek(Request $request, $id) {
        $this->validate($request, [
            'week_num' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'semester' => 'required',
        ]);

        $academicWeek = AcademicWeek::find($id);
        $academicWeek->week_num = $request->week_num;
        $academicWeek->start_date = Carbon::parse($request->start_date);
        $academicWeek->end_date = Carbon::parse($request->end_date);
        $academicWeek->semester = $request->semester;
        $academicWeek->save();

        return response()->json([
            'academicWeek' => $academicWeek,
            'status' => true
        ], 200);
    }

    public function deleteAcademicWeek(Request $request, $id) {
        $academicWeek = AcademicWeek::find($id);
        $academicWeek->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
