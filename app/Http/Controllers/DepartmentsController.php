<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    public function getDepartmentById(Request $request, $id) {
        return response()->json([
            'data' => Department::where(['id' => $id])->with('auditories')->with('lecturers')->first(),
            'status' => true,
        ], 200);
    }

    public function getDepartments(Request $request) {
        $departments = Department::with('auditories')->with('lecturers')->get();

        return response()->json([
            'data' => $departments,
            'status' => true
        ], 200);
    }

    public function createDepartment(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $department = new Department();
        $department->name = $request->name;
        $department->save();

        return response()->json([
            'department' => $department,
            'status' => true
        ], 200);
    }

    public function updateDepartment(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $department = Department::find($id);
        $department->name = $request->name;
        $department->save();

        return response()->json([
            'department' => $department,
            'status' => true
        ], 200);
    }

    public function deleteDepartment(Request $request, $id) {
        $department = Department::find($id);
        $department->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
