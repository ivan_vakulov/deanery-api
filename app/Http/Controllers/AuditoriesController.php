<?php

namespace App\Http\Controllers;

use App\Auditory;
use App\Department;
use Illuminate\Http\Request;

class AuditoriesController extends Controller
{
    public function getAuditoryById(Request $request, $id) {
        return response()->json([
            'data' => Auditory::where(['id' => $id])->with('department')->first(),
            'status' => true,
        ], 200);
    }

    public function getAuditories(Request $request) {
    $auditories = Auditory::with('department')->get();

    return response()->json([
        'data' => $auditories,
        'status' => true
    ], 200);
}

    public function createAuditory(Request $request) {
        $this->validate($request, [
            'number' => 'required',
            'building_number' => 'required',
            'capacity' => 'required',
            'department_id' => 'required',
        ]);

        $auditory = new Auditory();
        $auditory->number = $request->number;
        $auditory->building_number = $request->building_number;
        $auditory->capacity = $request->capacity;
        $auditory->department()->associate(Department::find($request->department_id));
        $auditory->save();

        return response()->json([
            'auditory' => $auditory,
            'status' => true
        ], 200);
    }

    public function updateAuditory(Request $request, $id) {
        $this->validate($request, [
            'number' => 'required',
            'building_number' => 'required',
            'capacity' => 'required',
            'department_id' => 'required',
        ]);

        $auditory = Auditory::find($id);
        $auditory->number = $request->number;
        $auditory->building_number = $request->building_number;
        $auditory->capacity = $request->capacity;
        $auditory->department()->associate(Department::find($request->department_id));
        $auditory->save();

        return response()->json([
            'auditory' => $auditory,
            'status' => true
        ], 200);
    }

    public function deleteAuditory(Request $request, $id) {
        $auditory = Auditory::find($id);
        $auditory->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
