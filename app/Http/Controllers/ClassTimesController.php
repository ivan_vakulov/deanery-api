<?php

namespace App\Http\Controllers;

use App\ClassTime;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClassTimesController extends Controller
{
    public function getClassTimeById(Request $request, $id) {
        return response()->json([
            'data' => ClassTime::where(['id' => $id])->first(),
            'status' => true,
        ], 200);
    }

    public function getClassTimes(Request $request) {
        $classTimes = ClassTime::all();

        return response()->json([
            'data' => $classTimes,
            'status' => true
        ], 200);
    }

    public function createClassTime(Request $request) {
        $this->validate($request, [
            'start_time' => 'required',
        ]);

        $classTime = new ClassTime();
        $classTime->start_time = $request->start_time;
        $classTime->end_time = $request->end_time;
        $classTime->save();

        return response()->json([
            'classTime' => $classTime,
            'status' => true
        ], 200);
    }

    public function updateClassTime(Request $request, $id) {
        $this->validate($request, [
            'start_time' => 'required',
        ]);

        $classTime = ClassTime::find($id);
        $classTime->start_time = $request->start_time;
        $classTime->end_time = $request->end_time;
        $classTime->save();

        return response()->json([
            'classTime' => $classTime,
            'status' => true
        ], 200);
    }

    public function deleteClassTime(Request $request, $id) {
        $classTime = ClassTime::find($id);
        $classTime->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
