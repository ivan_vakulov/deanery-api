<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\Specialty;
use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    public function getDisciplineById(Request $request, $id) {
        return response()->json([
            'data' => Discipline::where(['id' => $id])->with('preDiscipline')->with('specialty')->first(),
            'status' => true,
        ], 200);
    }

    public function getDisciplines(Request $request) {
        $disciplines = Discipline::with('preDiscipline')->with('specialty')->get();

        return response()->json([
            'data' => $disciplines,
            'status' => true
        ], 200);
    }

    public function createDiscipline(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'selfWorkTime' => 'required',
            'specialty_id' => 'required',
        ]);

        $discipline = new Discipline();
        $discipline->name = $request->name;
        $discipline->selfWorkTime = $request->selfWorkTime;
        $discipline->specialty()->associate(Specialty::find($request->specialty_id));
        $discipline->preDiscipline()->associate(Discipline::find($request->pre_discipline_id));
        $discipline->save();

        return response()->json([
            'discipline' => $discipline,
            'status' => true
        ], 200);
    }

    public function updateDiscipline(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'selfWorkTime' => 'required',
            'specialty_id' => 'required',
        ]);

        $discipline = Discipline::find($id);
        $discipline->name = $request->name;
        $discipline->selfWorkTime = $request->selfWorkTime;
        $discipline->specialty()->associate(Specialty::find($request->specialty_id));
        $discipline->preDiscipline()->associate(Discipline::find($request->pre_discipline_id));
        $discipline->save();

        return response()->json([
            'discipline' => $discipline,
            'status' => true
        ], 200);
    }

    public function deleteDiscipline(Request $request, $id) {
        $discipline = Discipline::find($id);
        $discipline->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
