<?php

namespace App\Http\Controllers;

use App\Specialty;
use Illuminate\Http\Request;

class SpecialtiesController extends Controller
{
    public function getSpecialtyById(Request $request, $id) {
        return response()->json([
            'data' => Specialty::where(['id' => $id])->first(),
            'status' => true,
        ], 200);
    }

    public function getSpecialties(Request $request) {
        $specialties = Specialty::all();

        return response()->json([
            'data' => $specialties,
            'status' => true
        ], 200);
    }

    public function createSpecialty(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:specialties',
        ]);

        $specialty = new Specialty();
        $specialty->name = $request->name;
        $specialty->save();

        return response()->json([
            'specialty' => $specialty,
            'status' => true
        ], 200);
    }

    public function updateSpecialty(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|unique:specialties',
        ]);

        $specialty = Specialty::find($id);
        $specialty->name = $request->name;
        $specialty->save();

        return response()->json([
            'specialty' => $specialty,
            'status' => true
        ], 200);
    }

    public function deleteSpecialty(Request $request, $id) {
        $specialty = Specialty::find($id);
        $specialty->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
