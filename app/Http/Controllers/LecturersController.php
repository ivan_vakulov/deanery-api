<?php

namespace App\Http\Controllers;

use App\Department;
use App\Lecturer;
use Illuminate\Http\Request;

class LecturersController extends Controller
{
    public function getLecturerById(Request $request, $id) {
        return response()->json([
            'data' => Lecturer::where(['id' => $id])->with('department')->first(),
            'status' => true,
        ], 200);
    }

    public function getLecturers(Request $request) {
        $lecturers = Lecturer::with('department')->get();

        return response()->json([
            'data' => $lecturers,
            'status' => true
        ], 200);
    }

    public function createLecturer(Request $request) {
        $this->validate($request, [
            'full_name' => 'required',
            'position' => 'required',
            'phone_number' => 'required',
            'department_id' => 'required',
        ]);

        $lecturer = new Lecturer();
        $lecturer->full_name = $request->full_name;
        $lecturer->position = $request->position;
        $lecturer->phone_number = $request->phone_number;
        $lecturer->department()->associate(Department::find($request->department_id));
        $lecturer->save();

        return response()->json([
            'lecturer' => $lecturer,
            'status' => true
        ], 200);
    }

    public function updateAuditory(Request $request, $id) {
        $this->validate($request, [
            'full_name' => 'required',
            'position' => 'required',
            'phone_number' => 'required',
            'department_id' => 'required',
        ]);

        $lecturer = Lecturer::find($id);
        $lecturer->full_name = $request->full_name;
        $lecturer->position = $request->position;
        $lecturer->phone_number = $request->phone_number;
        $lecturer->department()->associate(Department::find($request->department_id));
        $lecturer->save();

        return response()->json([
            'lecturer' => $lecturer,
            'status' => true
        ], 200);
    }

    public function deleteLecturer(Request $request, $id) {
        $lecturer = Lecturer::find($id);
        $lecturer->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
