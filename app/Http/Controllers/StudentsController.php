<?php

namespace App\Http\Controllers;

use App\Specialty;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function getStudentById(Request $request, $id) {
        return response()->json([
            'data' => Student::where(['id' => $id])->with('specialty')->first(),
            'status' => true,
        ], 200);
    }

    public function getStudents(Request $request) {
        $students = Student::with('specialty')->get();

        return response()->json([
            'data' => $students,
            'status' => true
        ], 200);
    }

    public function createStudent(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'start_university' => 'required',
            'end_university' => 'required',
            'phone' => 'required',
            'end_reason' => 'required',
            'specialty_id' => 'required',
        ]);

        $student = new Student();
        $student->name = $request->name;
        $student->start_university = Carbon::parse($request->start_university);
        $student->end_university = Carbon::parse($request->end_university);
        $student->phone = $request->phone;
        $student->end_reason = $request->end_reason;
        $student->specialty()->associate(Specialty::find($request->specialty_id));
        $student->save();

        return response()->json([
            'student' => $student,
            'status' => true
        ], 200);
    }

    public function updateStudent(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'start_university' => 'required',
            'end_university' => 'required',
            'phone' => 'required',
            'end_reason' => 'required',
            'specialty_id' => 'required',
        ]);

        $student = Student::find($id);
        $student->name = $request->name;
        $student->start_university = Carbon::parse($request->start_university);
        $student->end_university = Carbon::parse($request->end_university);
        $student->phone = $request->phone;
        $student->end_reason = $request->end_reason;
        $student->specialty()->associate(Specialty::find($request->specialty_id));
        $student->save();

        return response()->json([
            'student' => $student,
            'status' => true
        ], 200);
    }

    public function deleteStudent(Request $request, $id) {
        $student = Student::find($id);
        $student->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
