<?php

namespace App\Http\Controllers;

use App\UniversityGroup;
use Illuminate\Http\Request;

class UniversityGroupsController extends Controller
{
    public function getUniversityGroupById(Request $request, $id) {
        return response()->json([
            'data' => UniversityGroup::where(['id' => $id])->with('universitySchedules')->first(),
            'status' => true,
        ], 200);
    }

    public function getUniversityGroups(Request $request) {
        $universityGroups = UniversityGroup::with('universitySchedules')->get();

        return response()->json([
            'data' => $universityGroups,
            'status' => true
        ], 200);
    }

    public function createUniversityGroup(Request $request) {
        $this->validate($request, [
            'group_number' => 'required',
        ]);

        $universityGroup = new UniversityGroup();
        $universityGroup->group_number = $request->group_number;
        $universityGroup->save();

        return response()->json([
            'universityGroup' => $universityGroup,
            'status' => true
        ], 200);
    }

    public function updateUniversityGroup(Request $request, $id) {
        $this->validate($request, [
            'group_number' => 'required',
        ]);

        $universityGroup = UniversityGroup::find($id);
        $universityGroup->group_number = $request->group_number;
        $universityGroup->save();

        return response()->json([
            'universityGroup' => $universityGroup,
            'status' => true
        ], 200);
    }

    public function deleteUniversityGroup(Request $request, $id) {
        $universityGroup = UniversityGroup::find($id);
        $universityGroup->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
