<?php

namespace App\Http\Controllers;

use App\AcademicWeek;
use App\Auditory;
use App\ClassTime;
use App\UniversityClass;
use App\UniversityGroup;
use App\UniversitySchedule;
use Illuminate\Http\Request;

class UniversitySchedulesController extends Controller
{
    public function getUniversityScheduleById(Request $request, $id) {
        return response()->json([
            'data' => UniversitySchedule::where(['id' => $id])->with('classTime')->with('academicWeek')->with('auditory')->with('universityClass')->with('group')->first(),
            'status' => true,
        ], 200);
    }

    public function getUniversitySchedules(Request $request) {
        $universitySchedules = UniversitySchedule::with('classTime')->with('academicWeek')->with('auditory')->with('universityClass')->with('group')->get();

        return response()->json([
            'data' => $universitySchedules,
            'status' => true
        ], 200);
    }

    public function createUniversitySchedule(Request $request) {
        $this->validate($request, [
            'week_day' => 'required',
            'class_time_id' => 'required',
            'academic_week_id' => 'required',
            'auditory_id' => 'required',
            'university_class_id' => 'required',
            'university_group_id' => 'required',
        ]);

        $universitySchedule = new UniversitySchedule();
        $universitySchedule->week_day = $request->week_day;
        $universitySchedule->classTime()->associate(ClassTime::find($request->class_time_id));
        $universitySchedule->academicWeek()->associate(AcademicWeek::find($request->academic_week_id));
        $universitySchedule->auditory()->associate(Auditory::find($request->auditory_id));
        $universitySchedule->universityClass()->associate(UniversityClass::find($request->university_class_id));
        $universitySchedule->group()->associate(UniversityGroup::find($request->university_group_id));
        $universitySchedule->save();

        return response()->json([
            'universitySchedule' => $universitySchedule,
            'status' => true
        ], 200);
    }

    public function updateUniversitySchedule(Request $request, $id) {
        $this->validate($request, [
            'week_day' => 'required',
            'class_time_id' => 'required',
            'academic_week_id' => 'required',
            'auditory_id' => 'required',
            'university_class_id' => 'required',
            'university_group_id' => 'required',
        ]);

        $universitySchedule = UniversitySchedule::find($id);
        $universitySchedule->week_day = $request->week_day;
        $universitySchedule->classTime()->associate(ClassTime::find($request->class_time_id));
        $universitySchedule->academicWeek()->associate(AcademicWeek::find($request->academic_week_id));
        $universitySchedule->auditory()->associate(Auditory::find($request->auditory_id));
        $universitySchedule->universityClass()->associate(UniversityClass::find($request->university_class_id));
        $universitySchedule->group()->associate(UniversityGroup::find($request->university_group_id));
        $universitySchedule->save();

        return response()->json([
            'universitySchedule' => $universitySchedule,
            'status' => true
        ], 200);
    }

    public function deleteUniversitySchedule(Request $request, $id) {
        $universitySchedule = UniversitySchedule::find($id);
        $universitySchedule->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
