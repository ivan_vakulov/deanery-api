<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\Lecturer;
use App\UniversityClass;
use App\UniversitySchedule;
use Illuminate\Http\Request;

class UniversityClassesController extends Controller
{
    public function getUniversityClassById(Request $request, $id) {
        return response()->json([
            'data' => UniversityClass::where(['id' => $id])->with('discipline')->with('universitySchedule')->with('lecturer')->first(),
            'status' => true,
        ], 200);
    }

    public function getUniversityClasses(Request $request) {
        $universityClasses = UniversityClass::with('discipline')->with('universitySchedule')->with('lecturer')->get();

        return response()->json([
            'data' => $universityClasses,
            'status' => true
        ], 200);
    }

    public function createUniversityClass(Request $request) {
        $this->validate($request, [
            'class_type' => 'required',
            'discipline_id' => 'required',
            'lecturer_id' => 'required',
        ]);

        $universityClass = new UniversityClass();
        $universityClass->class_type = $request->class_type;
        $universityClass->discipline()->associate(Discipline::find($request->discipline_id));
        $universityClass->lecturer()->associate(Lecturer::find($request->lecturer_id));
        $universityClass->universitySchedule()->associate(UniversitySchedule::find($request->university_schedule_id));
        $universityClass->save();

        return response()->json([
            'universityClass' => $universityClass,
            'status' => true
        ], 200);
    }

    public function updateUniversityClass(Request $request, $id) {
        $this->validate($request, [
            'class_type' => 'required',
            'discipline_id' => 'required',
            'lecturer_id' => 'required',
        ]);

        $universityClass = UniversityClass::find($id);
        $universityClass->class_type = $request->class_type;
        $universityClass->discipline()->associate(Discipline::find($request->discipline_id));
        $universityClass->lecturer()->associate(Lecturer::find($request->lecturer_id));
        $universityClass->universitySchedule()->associate(UniversitySchedule::find($request->university_schedule_id));
        $universityClass->save();

        return response()->json([
            'universityClass' => $universityClass,
            'status' => true
        ], 200);
    }

    public function deleteUniversityClass(Request $request, $id) {
        $universityClass = UniversityClass::find($id);
        $universityClass->delete();

        return response()->json([
            'status' => true
        ], 200);
    }
}
