<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    public function specialty() {
        return $this->belongsTo('App\Specialty', 'specialty_id', 'id');
    }

    public function preDiscipline() {
        return $this->belongsTo('App\Discipline', 'pre_discipline_id', 'id');
    }

    public function discipline() {
        return $this->hasOne('App\Discipline', 'id', 'pre_discipline_id');
    }
}
