<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversitySchedule extends Model
{
    public function classTime() {
        return $this->belongsTo('App\ClassTime', 'class_time_id', 'id');
    }

    public function academicWeek() {
        return $this->belongsTo('App\AcademicWeek', 'academic_week_id', 'id');
    }

    public function auditory() {
        return $this->belongsTo('App\Auditory', 'auditory_id', 'id');
    }

    public function universityClass() {
        return $this->belongsTo('App\UniversityClass', 'university_class_id', 'id');
    }

    public function group() {
        return $this->belongsTo('App\UniversityGroup', 'university_group_id', 'id');
    }
}
