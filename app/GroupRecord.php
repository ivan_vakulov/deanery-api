<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupRecord extends Model
{
    public function student() {
        return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function universityGroup() {
        return $this->belongsTo('App\UniversityGroup', 'university_group_id', 'id');
    }

    public function discipline() {
        return $this->belongsTo('App\Discipline', 'discipline_id', 'id');
    }
}
