<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function specialty() {
        return $this->belongsTo('App\Specialty', 'specialty_id', 'id');
    }
}
