<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityClass extends Model
{
    public function discipline() {
        return $this->belongsTo('App\Discipline', 'discipline_id', 'id');
    }

    public function lecturer() {
        return $this->belongsTo('App\Lecturer', 'lecturer_id', 'id');
    }

    public function universitySchedule() {
        return $this->hasOne('App\UniversitySchedule', 'university_class_id', 'id');
    }
}
