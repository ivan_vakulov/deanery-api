<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function() {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

});

Route::group(['middleware' => 'auth', 'prefix' => 'departments'], function() {

    Route::get('{id}', 'DepartmentsController@getDepartmentById');
    Route::get('', 'DepartmentsController@getDepartments');
    Route::post('', 'DepartmentsController@createDepartment');
    Route::post('{id}', 'DepartmentsController@updateDepartment');
    Route::delete('{id}', 'DepartmentsController@deleteDepartment');

});

Route::group(['middleware' => 'auth', 'prefix' => 'auditories'], function() {

    Route::get('{id}', 'AuditoriesController@getAuditoryById');
    Route::get('', 'AuditoriesController@getAuditories');
    Route::post('', 'AuditoriesController@createAuditory');
    Route::post('{id}', 'AuditoriesController@updateAuditory');
    Route::delete('{id}', 'AuditoriesController@deleteAuditory');

});

Route::group(['middleware' => 'auth', 'prefix' => 'lecturers'], function() {

    Route::get('{id}', 'LecturersController@getLecturerById');
    Route::get('', 'LecturersController@getLecturers');
    Route::post('', 'LecturersController@createLecturer');
    Route::post('{id}', 'LecturersController@updateAuditory');
    Route::delete('{id}', 'LecturersController@deleteLecturer');

});

Route::group(['middleware' => 'auth', 'prefix' => 'specialties'], function() {

    Route::get('{id}', 'SpecialtiesController@getSpecialtyById');
    Route::get('', 'SpecialtiesController@getSpecialties');
    Route::post('', 'SpecialtiesController@createSpecialty');
    Route::post('{id}', 'SpecialtiesController@updateSpecialty');
    Route::delete('{id}', 'SpecialtiesController@deleteSpecialty');

});

Route::group(['middleware' => 'auth', 'prefix' => 'disciplines'], function() {

    Route::get('{id}', 'DisciplineController@getDisciplineById');
    Route::get('', 'DisciplineController@getDisciplines');
    Route::post('', 'DisciplineController@createDiscipline');
    Route::post('{id}', 'DisciplineController@updateDiscipline');
    Route::delete('{id}', 'DisciplineController@deleteDiscipline');

});

Route::group(['middleware' => 'auth', 'prefix' => 'university-classes'], function() {

    Route::get('{id}', 'UniversityClassesController@getUniversityClassById');
    Route::get('', 'UniversityClassesController@getUniversityClasses');
    Route::post('', 'UniversityClassesController@createUniversityClass');
    Route::post('{id}', 'UniversityClassesController@updateUniversityClass');
    Route::delete('{id}', 'UniversityClassesController@deleteUniversityClass');

});

Route::group(['middleware' => 'auth', 'prefix' => 'academic-weeks'], function() {

    Route::get('{id}', 'AcademicWeeksController@getAcademicWeekById');
    Route::get('', 'AcademicWeeksController@getAcademicWeeks');
    Route::post('', 'AcademicWeeksController@createAcademicWeek');
    Route::post('{id}', 'AcademicWeeksController@updateAcademicWeek');
    Route::delete('{id}', 'AcademicWeeksController@deleteAcademicWeek');

});

Route::group(['middleware' => 'auth', 'prefix' => 'class-times'], function() {

    Route::get('{id}', 'ClassTimesController@getClassTimeById');
    Route::get('', 'ClassTimesController@getClassTimes');
    Route::post('', 'ClassTimesController@createClassTime');
    Route::post('{id}', 'ClassTimesController@updateClassTime');
    Route::delete('{id}', 'ClassTimesController@deleteClassTime');

});

Route::group(['middleware' => 'auth', 'prefix' => 'university-schedules'], function() {

    Route::get('{id}', 'UniversitySchedulesController@getUniversityScheduleById');
    Route::get('', 'UniversitySchedulesController@getUniversitySchedules');
    Route::post('', 'UniversitySchedulesController@createUniversitySchedule');
    Route::post('{id}', 'UniversitySchedulesController@updateUniversitySchedule');
    Route::delete('{id}', 'UniversitySchedulesController@deleteUniversitySchedule');

});

Route::group(['middleware' => 'auth', 'prefix' => 'students'], function() {

    Route::get('{id}', 'StudentsController@getStudentById');
    Route::get('', 'StudentsController@getStudents');
    Route::post('', 'StudentsController@createStudent');
    Route::post('{id}', 'StudentsController@updateStudent');
    Route::delete('{id}', 'StudentsController@deleteStudent');

});

Route::group(['middleware' => 'auth', 'prefix' => 'university-groups'], function() {

    Route::get('{id}', 'UniversityGroupsController@getUniversityGroupById');
    Route::get('', 'UniversityGroupsController@getUniversityGroups');
    Route::post('', 'UniversityGroupsController@createUniversityGroup');
    Route::post('{id}', 'UniversityGroupsController@updateUniversityGroup');
    Route::delete('{id}', 'UniversityGroupsController@deleteUniversityGroup');

});

Route::group(['middleware' => 'auth', 'prefix' => 'group-records'], function() {

    Route::get('{id}', 'GroupRecordsController@getGroupRecordById');
    Route::get('', 'GroupRecordsController@getGroupRecords');
    Route::post('', 'GroupRecordsController@createGroupRecord');
    Route::post('{id}', 'GroupRecordsController@updateGroupRecord');
    Route::delete('{id}', 'GroupRecordsController@deleteGroupRecord');

});